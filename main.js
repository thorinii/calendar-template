const year = 2023
const flowInRows = false
const addDayOfWeekLabels = true
const daysPerColumn = 7 * 5;
const daysPerRow = 14;


document.title = document.title.replace('[year]', year);


const startDate = new Date()
startDate.setHours(0, 0, 0, 0)
startDate.setFullYear(year, 0, 1)

const days = [];
while (startDate.getFullYear() === year) {
  days.push({
    season: (((13 + startDate.getMonth()) % 12) / 3) | 0,
    month: startDate.getMonth(),
    date: startDate.getDate(),
    dayOfWeek: startDate.getDay()
  })
  startDate.setDate(startDate.getDate() + 1)
}


// layout algorithm
let templateRows;
if (flowInRows) {
  var currentRow = [];
  templateRows = [currentRow];

  // add empty cells for last year
  for (var i = 0; i < days[0].dayOfWeek; i++) {
    currentRow.push(null);
  }
  days.forEach(function (day) {
    if (currentRow.length >= daysPerRow) {
      currentRow = [];
      templateRows.push(currentRow);
    }
    currentRow.push(day);
  });

  // add empty cells for next year to fill row
  for (var i = currentRow.length; i < daysPerRow; i++) {
    currentRow.push(null);
  }
} else {
  var currentColumn = [];
  const columns = [currentColumn];

  // add empty cells for last year
  for (var i = 0; i < days[0].dayOfWeek; i++) {
    currentColumn.push(null);
  }
  days.forEach(function (day) {
    if (currentColumn.length >= daysPerColumn) {
      currentColumn = [];
      columns.push(currentColumn);
    }
    currentColumn.push(day);
  });

  // add empty cells for next year to fill column
  for (var i = currentColumn.length; i < daysPerColumn; i++) {
    currentColumn.push(null);
  }

  // reshape from columns/rows to rows/columns
  templateRows = Array(columns[0].length).fill(null).map(() => [])
  columns.forEach((column, x) => {
    column.forEach((cell, y) => {
      if (!templateRows[y]) templateRows[y] = []
      templateRows[y].push(cell)
    })
  })
}


const seasons = ['summer', 'autumn', 'winter', 'spring']
const monthsShort = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
const daysOfWeek = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']


if (addDayOfWeekLabels) {
  if (flowInRows) {
    const row = templateRows[0].map((c, i) => {
      return {
        type: 'dow',
        day: i % daysOfWeek.length
      };
    })
    templateRows.unshift(row)
  } else {
    templateRows.forEach((row, i) => {
      row.unshift({
        type: 'dow',
        day: i % daysOfWeek.length
      });
    })
  }
}


const pageWidthMillis = 400;
const pageHeightMillis = 250;
const columns = templateRows[0].length;
const rows = templateRows.length;
const cellWidthMillis = pageWidthMillis / columns;
const cellHeightMillis = pageHeightMillis / rows;

const calenderEl = document.querySelector('.calendar');

templateRows.forEach(function (row, idx) {
  if (idx !== 0) {
    calenderEl.appendChild(document.createElement('br'))
  }

  row.forEach(function (cell) {
    const el = document.createElement('div')
    el.classList.add('cell')

    if (cell === null) {

    } else if (cell.type === 'dow') {
      el.classList.add('day-' + daysOfWeek[cell.day])
      const text = daysOfWeek[cell.day].slice(0, 3).toUpperCase()
      el.appendChild(label('label-dow', text));
    } else {
      const day = cell;
      el.classList.add('season-' + seasons[day.season])
      el.classList.add('month-' + monthsShort[day.month])
      el.classList.add('day-' + daysOfWeek[day.dayOfWeek])
      el.classList.add('date-' + day.date)

      el.classList.add('season-' + (day.season % 2 === 0 ? 'even' : 'odd'))
      el.classList.add('month-' + (day.month % 2 === 0 ? 'even' : 'odd'))

      if (day.date === 1) {
        el.appendChild(label('label-date', monthsShort[day.month].toUpperCase()));
      } else {
        el.appendChild(label('label-date', day.date));
      }
    }

    el.style.width = cellWidthMillis + 'mm';
    el.style.height = cellHeightMillis + 'mm';

    calenderEl.appendChild(el)
  })
})


function obj (className) {
  const el = document.createElement('div')
  el.classList.add(className)
  return el
}
function label (className, text) {
  const el = document.createElement('div')
  el.classList.add(className)
  el.innerText = text
  return el
}
